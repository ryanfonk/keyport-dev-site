<?php get_header(); ?>
		
		<div class="wrapper">
		
			<h1 class="page-title"><?php single_cat_title(); ?></h1>
		
		</div>
		
		<?php if ( have_posts() ) : ?>
	
			<?php while ( have_posts() ) : the_post(); ?>
		<div class="item">
			<div class="wrapper">
		
				<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		
				<p><?php the_field('price_text'); ?></p>
				<p><?php the_field('id'); ?></p>
				<p><?php the_field('description'); ?></p>
				
	
			</div>
		</div>


						
			<?php endwhile; ?>
	
	
		<div id="pagination">
			<div class="wrapper">
				<div class="prev">
					<?php previous_posts_link('&laquo; Previous') ?>
				</div>
				
				<div class="next">
					<?php next_posts_link('Next &raquo;') ?>
				</div>
				

			</div>
		</div>
			<?php endif; ?>


<?php get_footer(); ?>
