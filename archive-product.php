<?php get_header(); ?>


	<section id="hero" class="backstretch" data-img-src="<?php the_field('products_background', 'options'); ?>">

		<div class="wrapper">

			<h1><?php the_field('products_headline', 'options'); ?></h1>
			<h3><?php the_field('products_subheader', 'options'); ?></h3>

		</div>

	</section>

	<section class="products">

		<div class="wrapper title">

			<h2>Our products</h2>

		</div>

		<?php include('inc/products.php'); ?>

	</section>


<?php get_footer(); ?>
