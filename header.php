<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<script src="//use.typekit.net/ltl8haa.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>

		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/slick.css" />
		<link href="<?php bloginfo('template_directory'); ?>/css/jquery.bxslider.css" rel="stylesheet" />

		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?> id="<?php echo the_slug();?>">

		<header class="backstretch" data-img-src="<?php $image = get_field('header_background', 'options'); echo $image['url']; ?>">			<div class="wrapper">



				<div id="crab"></div>

				<a href="<?php echo site_url('/'); ?>" id="logo"><img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>

				<a href="#" id="mobile-nav">☰</a>

				<div id="header-contact">
					<h4>
						<a href="<?php echo site_url('/contact/'); ?>">Contact Us</a><br/>
						<?php the_field('phone', 'options'); ?>
					</h4>
				</div>

			</div>
		</header>

		<nav>
			<div class="wrapper">

				<div id="sticky-logo">

					<a href="<?php echo site_url('/'); ?>" id="logo"><img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
				</div>

			</div>

			<ul>
				<li><a href="<?php echo site_url('/retail/'); ?>">Retail</a></li>

				<li><a href="<?php echo site_url('/wholesale/'); ?>">Wholesale</a></li>

				<li><a href="<?php echo site_url('/about/'); ?>">About</a></li>

				<li><a href="<?php echo site_url('/product/'); ?>">Products</a>

					<div class="dropdown-wrapper">

						<div class="wrapper">

							<div class="nav-title"><a href="<?php echo site_url('/'); ?>/product">All products</a></div>

						</div>

						<div class="wrapper">

							<?php if(have_rows('product_display', 'options')): while(have_rows('product_display', 'options')) : the_row(); ?>

							            <?php if( get_row_layout() == 'categories' ): ?>

							            	<div class="nav-column">

							                            <h4><?php the_sub_field('title'); ?></h4>

								                        <ul>

								                            <?php $posts = get_sub_field('product_selection'); if( $posts ): ?>

								                                <?php foreach( $posts as $post):  ?>

								                                    <?php setup_postdata($post); ?>

								                                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

								                                    <?php endforeach; ?>

								                                <?php wp_reset_postdata(); ?>

								                            <?php endif; ?>

								                        </ul>
							                        </div>

							            <?php endif; ?>

							<?php endwhile; endif; ?>

					            	<div class="cooking-column">

					                            <h4><a href="<?php echo site_url('/'); ?>/instructions">Cooking instructions</a></h4>

					                        </div>

						</div>
					</div>

				</li>

				<li><a href="<?php echo site_url('/contact/'); ?>">Contact Us</a></li>

				<li><a href="http://www.keyportllc.com/blog/">Blog</a>

			</ul>

		</nav>
