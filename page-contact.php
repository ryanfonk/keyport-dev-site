<?php get_header(); ?>

	<section id="hero" class="backstretch" data-img-src="<?php $image = get_field('hero_background'); echo $image['url']; ?>">
		<div class="wrapper">

			<div class="info">
				<h1><?php the_field('hero_headline'); ?></h1>
			</div>

		</div>
	</section>

	<section id="info">
		<div class="wrapper">

			<?php the_field('contact_information'); ?>

		</div>
	</section>


	<section id="form">
		<div class="wrapper">

			<?php the_field('form_info'); ?>

			<?php echo do_shortcode('[contact-form-7 id="153" title="Contact Form"]'); ?>

		</div>
	</section>

<?php get_footer(); ?>
