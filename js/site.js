function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}


var pathname = location.pathname;
var pathArray = window.location.pathname.split( '/' );
var firstLevelLocation = pathArray[2];
var highlight;

$(function() {

    // First Level
    if(pathname == "/keyport/")
        highlight = $('header #logo');
    else {
        highlight = $('nav a[href$="/' + firstLevelLocation + '/"]');
    }

    highlight.addClass('active');
});

$(document).ready(function(){
  $('.bxslider').bxSlider({
            minSlides: 1,
            maxSlides: 3,
            moveSlides:1,
            slideWidth: 320,
            slideMargin: 20,
            infiniteLoop: true

});

});

$(".category").hover(function(){

    var imgurl = $(this).data("hoverimage");
    $(this).css("background-image", "url(" + imgurl + ")")
}, function(){
    $(this).css("background-image", "");
});

$(document).ready(function() {


$('.nl-title').click( function() {
    $("#footer-email-capture").toggleClass("open");
    $('.nl-title img').toggleClass('rotated');
} );

});


$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	$('#mobile-nav').click( function() {
		$('nav').slideToggle('slow');
		return false;
	});


    // Backstretch
	$('.backstretch').each(function() {
		var bgImage = $(this).attr('data-img-src');

		if(bgImage) {
			$(this).backstretch(bgImage);
		}
	});


	$('#contact #form .company-type select option:contains("---")').text('-- Company Type --');


	var productList = $('#offerings .products-list > .product');
	$('#offerings #filters a').click(function(){

		var filterTarget = '#offerings .products-list > ' + $(this).attr('data-filter');
		$(productList).removeClass('include');
		$(filterTarget).addClass('include');

		$('#offerings #filters a').removeClass('active');
		$(this).addClass('active');

		return false;

	}).filter(':first').click();


	$('#other-products #products-wrapper').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: "<a href='#' class='ir slick-prev'>Previous</a>",
		nextArrow: "<a href='#' class='ir slick-next'>Next</a>",
		dots: true,

	});


	$('.single-product #gallery').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: "<a href='#' class='ir slick-prev'>Previous</a>",
		nextArrow: "<a href='#' class='ir slick-next'>Next</a>",
		dots: true,

	});

            $('.center').slick({
              centerMode: true,
              centerPadding: '60px',
              slidesToShow: 3,
              responsive: [
                {
                  breakpoint: 1150,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    dots: true,
                    slidesToShow: 2
                  }
                },
                {
                  breakpoint: 680,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    dots: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                  }
                }
              ]
            });

            $('.center2').slick({
              centerMode: true,
              centerPadding: '60px',
              slidesToShow: 3,
              responsive: [
                {
                  breakpoint: 1150,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    dots: true,
                    slidesToShow: 2
                  }
                },
                {
                  breakpoint: 680,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    dots: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                  }
                }
              ]
            });

                $('.Fish1').addClass('active');

                $(".recipe-card").each(function(){
                   $(this).hide();
                  if($(this).attr('id') == 'section-Fish1') {
                      $(this).show();
                  }
              });

              $('.recipe-link').on( "click", function(e) {
                  e.preventDefault();
                  var id = $(this).attr('data-related');
                  $(".recipe-card").each(function(){
                      $(this).hide();
                      if($(this).attr('id') == id) {
                          $(this).show();
                      }
                  });
              });

             $(".recipe-titles a").on("click", function(e) {
                  e.stopPropagation();
                  $(".active").removeClass("active");
                  $(this).addClass("active");
              });


	//Sticky Nav
    var sticky_navigation_offset_top = $('nav').offset().top;

    var sticky_navigation = function(){
        var scroll_top = $(window).scrollTop();

        if (scroll_top > sticky_navigation_offset_top) {
            $('nav').addClass('sticky');
        } else {
            $('nav').removeClass('sticky');
        }
    };

    sticky_navigation();

    $(window).scroll(function() {
         sticky_navigation();
    });


});
