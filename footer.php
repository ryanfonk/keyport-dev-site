		<footer>

			<div class="wrapper">

				<div id="footer-contact-info">

					<?php the_field('footer_contact_info', 'options'); ?>

				</div>

				<div id="footer-nav">

					<ul class="pages">

						<?php if(get_field('navigation', 'options')): while(has_sub_field('navigation', 'options')): ?>

							<li><a href="<?php echo site_url(); ?><?php the_sub_field('url'); ?>"><?php the_sub_field('label'); ?></a></li>

						<?php endwhile; endif; ?>

					</ul>

					<ul class="products">

						<?php $args = array('post_type' => 'product'); ?>

						<?php $loop = new WP_Query($args); ?>

							<?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

						<?php endwhile; endif; ?>

					</ul>
				</div>

			</div>

			<section id="copyright">

				<div class="wrapper">

					<p><?php the_field('copyright', 'options'); ?></p>

				</div>

			</section>

			<div id="footer-email-capture">

				<div class="nl-title"><img src="http://www.keyportllc.com/wp-content/uploads/2016/06/x-btn.png"> <h4>Subscribe to news & promotions</h4></div>

				<div class="nl-content"><?php echo do_shortcode('[contact-form-7 id="126" title="Email Capture Form"]'); ?></div>

			</div>

		</footer>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>

		<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>

		<script src="<?php bloginfo('template_directory') ?>/js/jquery.bxslider.min.js"></script>

		<script>

$('.nl-title').click( function() {
    $("#footer-email-capture").toggleClass("open");
    $('.nl-title img').toggleClass('rotated');
} );
</script>

		<?php wp_footer(); ?>
<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-60619947-1', 'auto');
		  ga('send', 'pageview');

		</script>

	</body>
</html>
