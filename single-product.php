<?php get_header(); ?>

	<section id="hero" class="backstretch" data-img-src="<?php $image = get_field('hero'); echo $image['url']; ?>">
		<div class="wrapper">

			<div class="info">
				<div class="label-wrap">
					<?php the_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>

		</div>
	</section>

	<section id="headline">
		<div class="wrapper">

			<h3><?php the_field('headline'); ?></h3>

		</div>
	</section>


	<section id="info">
		<div class="wrapper">

			<div id="gallery">

				<?php if(get_field('gallery')): while(has_sub_field('gallery')): ?>

				    <div class="photo">
					    <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				        <p><?php the_sub_field('caption'); ?></p>
				    </div>

				<?php endwhile; endif; ?>

			</div>

			<div id="product-info">
				<div class="description">
					<?php the_field('description'); ?>
				</div>

				<div class="attributes">
					<?php if(get_field('attributes')): while(has_sub_field('attributes')): ?>

					    <div class="attr">
					        <h5><?php the_sub_field('header'); ?></h5>
					        <?php the_sub_field('deck'); ?>
					    </div>

					<?php endwhile; endif; ?>
				</div>

				<?php if(get_field('product_sheet')): ?>
					<a href="<?php the_field('product_sheet'); ?>" class="product-sheet">Download Product Sheet</a>
				<?php endif; ?>

				<a href="<?php echo site_url('/wholesale/'); ?>" class="wholesale-info">Wholesale Information</a>
			</div>

		</div>
	</section>


	<section id="other-products" class="products-list">
		<div class="wrapper">

			<h3>Other Products</h3>

			<div id="products-wrapper">

				<?php
					$thisProduct = $post->ID;
					$exclude_ids = array( $thisProduct );
				    $args = array( 'post_type' => 'product', 'posts_per_page' => 100, 'post__not_in' => $exclude_ids);
				    $the_query = new WP_Query( $args );
				    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<div class="product product-<?php echo the_slug();?>">
						<div class="product-wrapper">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php $image = get_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								<div class="overlay">
									<div class="label-wrap">
										<img class="label" src="<?php $image = get_field('label'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>

								</div>
							</a>
						</div>
					</div>

				<?php endwhile; endif; wp_reset_query(); ?>




			</div>

		</div>
	</section>




<?php get_footer(); ?>
