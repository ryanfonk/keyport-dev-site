<?php get_header(); ?>

	<section id="hero">

		<div class="hero-image">

			 <img src="<?php $image = get_field('hero_background'); echo $image['url']; ?>">

			<div class="info">

				<h1><?php the_field('hero_headline'); ?></h1>

			</div>

		</div>

	</section>

	<section id="info">

		<div class="wrapper">

			<?php the_field('wholesale_info'); ?>

		</div>

	</section>

	<section id="features">

		<div class="wrapper">

			<?php if(get_field('features')): while(has_sub_field('features')): ?>

			    <div class="feature">

				    <img class="icon" src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

				    <h4><?php the_sub_field('headline'); ?></h4>

				    <?php the_sub_field('deck'); ?>

			    </div>

			<?php endwhile; endif; ?>

		</div>

	</section>

	<section class="related-products">

		<div class="wrapper">

			<ul class="center2">


					<?php while(has_sub_field('gallery_wholesale')) { ?>
				<li>

						<div class="photo">

							<img alt="<?php $icon = get_sub_field('photo'); echo $icon['alt']; ?>" src="<?php $icon = get_sub_field('photo'); echo $icon['url']; ?>">

						</div>

				</li>
					<?php } ?>


			</ul>

		</div>

	</section>

	<section class="products">

		<?php include('inc/products.php'); ?>

	</section>

<?php get_footer(); ?>
