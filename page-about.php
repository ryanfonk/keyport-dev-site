<?php get_header(); ?>

	<section id="hero" class="backstretch" data-img-src="<?php $image = get_field('hero_background'); echo $image['url']; ?>">
		<div class="wrapper">

			<div class="info">
				<h1><?php the_field('hero_headline'); ?></h1>
			</div>

		</div>
	</section>

	<section id="info">
		<div class="wrapper">

			<?php the_field('about_information'); ?>

		</div>
	</section>


	<section id="story">
		<div class="wrapper">

			<div class="photos">
				<img src="<?php $image = get_field('story_photo_1'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

				<img src="<?php $image = get_field('story_photo_2'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="text">
				<?php the_field('story'); ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>
