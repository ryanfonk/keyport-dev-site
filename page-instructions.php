<?php get_header(); ?>

        <section id="hero">

            <div class="hero-image">

                <img src="<?php $image = get_field('hero_background'); echo $image['url']; ?>">

                <div class="info">

                    <h1><?php the_field('hero_headline'); ?></h1>

                </div>

            </div>

        </section>

        <section id="cooking-instructions">

            <div class="wrapper">

                <div class="recipe-titles">

                    <?php if( have_rows('recipes', $post->ID) ): ?>

                        <?php while( have_rows('recipes', $post->ID) ): the_row(); $i = 0;?>

                            <?php $title = get_sub_field('category_title'); ?>

                                <h3><?php echo $title; ?></h3>

                                <?php if( have_rows('products') ): ?>

                                    <ul>

                                        <?php while( have_rows('products') ): $i++;  the_row(); ?>

                                            <li><a href="" data-related="section-<?php echo $title . $i; ?>" class="recipe-link <?php echo $title . $i; ?>"><?php the_sub_field('recipe_title'); ?></a></li>

                                        <?php endwhile; ?>

                                    </ul>

                                <?php endif; ?>

                        <?php endwhile; ?>

                    <?php endif; ?>

                </div>

                <div class="recipes">

                   <?php if( have_rows('recipes', $post->ID) ): ?>

                        <?php while( have_rows('recipes', $post->ID) ): the_row(); $i = 0;?>

                            <?php $title = get_sub_field('category_title') ?>

                                <?php if( have_rows('products') ):?>

                                    <?php while( have_rows('products') ): $i++;  the_row(); ?>

                                            <div class="recipe-card" id="section-<?php echo $title . $i; ?>">

                                                <div class="recipe-header">

                                                    <h2><?php the_sub_field('recipe_title'); ?></h2>

                                                    <h3><?php the_sub_field('subheadline'); ?></h3>

                                                    <p><?php the_sub_field('more_details'); ?></p>

                                                </div>

                                                <?php if( have_rows('recipe_details') ):?>

                                                    <?php while( have_rows('recipe_details') ): the_row(); ?>

                                                            <div class="recipe-section">

                                                                    <?php if(get_sub_field('section_icon') ): ?>

                                                                        <div class="recipe-icon">

                                                                            <img src="<?php $image = the_sub_field('section_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                                                                        </div>

                                                                        <div class="section-details">

                                                                            <h4><?php the_sub_field('section_title'); ?></h4>

                                                                            <p><?php the_sub_field('section_description'); ?></p>

                                                                        </div>

                                                                    <?php endif; ?>

                                                                    <?php if(! get_sub_field('section_icon') ): ?>

                                                                        <div class="section-details full">

                                                                            <h4><?php the_sub_field('section_title'); ?></h4>

                                                                            <p><?php the_sub_field('section_description'); ?></p>

                                                                        </div>

                                                                    <?php endif; ?>

                                                            </div>

                                                    <?php endwhile; ?>

                                                <?php endif; ?>

                                            </div>

                                    <?php endwhile; ?>

                                <?php endif; ?>

                        <?php endwhile; ?>

                    <?php endif; ?>

                </div>

            </div>

        </section>

<?php get_footer(); ?>
