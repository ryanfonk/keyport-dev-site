<?php get_header(); ?>

	<section id="hero">

		<div class="hero-image">

			 <img src="<?php $image = get_field('hero_background'); echo $image['url']; ?>">

			<div class="info">

				<h1><?php the_field('hero_headline'); ?></h1>

			</div>

		</div>

	</section>

	<section id="info">

		<div class="wrapper">

			<h2><?php the_field('retail_info'); ?></h2>

		</div>

	</section>

	<section id="features">

		<div class="wrapper">

			<?php if(get_field('features')): while(has_sub_field('features')): ?>

			    <div class="feature">

				    <img class="icon" src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

				    <h4><?php the_sub_field('headline'); ?></h4>

				    <?php the_sub_field('deck'); ?>

			    </div>

			<?php endwhile; endif; ?>

		</div>

	</section>

	<section class="related-products">

		<div class="wrapper">

			<ul class="center">

				 <?php $posts = get_field('product_selector'); if( $posts ): ?>

					<?php foreach( $posts as $post):  ?>

				                        <?php setup_postdata($post); ?>

							<li>

								<?php if(get_field('gallery')): while(has_sub_field('gallery')): ?>

								    <div class="photo">

									    <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

								    </div>

								<?php endwhile; endif; ?>

							</li>

		                                    	<?php endforeach; ?>

		                                	<?php wp_reset_postdata(); ?>

		                         <?php endif; ?>

			</ul>

		</div>

	</section>

	<section class="products">

		<?php include('inc/products.php'); ?>

	</section>

<?php get_footer(); ?>
