
        <?php if(have_rows('product_display', 'options')): while(have_rows('product_display', 'options')) : the_row(); ?>

            <?php if( get_row_layout() == 'categories' ): ?>

                <div class="category" data-hoverimage="<?php the_sub_field('masthead'); ?>">

                    <div class="wrapper">

                        <div class="product-title">

                            <div class="product-title-img" style="background: url(<?php the_sub_field('icons'); ?>) no-repeat 50% 4px; height: 90px;"></div>

                            <div class="product-title-img-active" style="background: url(<?php the_sub_field('icon_active'); ?>) no-repeat 50% 4px; height: 90px;"></div>

                            <h3><?php the_sub_field('title'); ?></h3>

                        </div>

                        <div class="product-list">

                            <?php $posts = get_sub_field('product_selection'); if( $posts ): ?>

                                <?php foreach( $posts as $post):  ?>

                                    <?php setup_postdata($post); ?>

                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                    <?php endforeach; ?>

                                <?php wp_reset_postdata(); ?>

                            <?php endif; ?>

                        </div>

                    </div>

                </div>

            <?php endif; ?>

        <?php endwhile; endif; ?>
