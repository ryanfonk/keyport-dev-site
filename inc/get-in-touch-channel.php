	<section id="get-in-touch" class="channel backstretch" data-img-src="<?php $image = get_field('get_in_touch_background', 'options'); echo $image['url']; ?>">
		<div class="wrapper">
			
			<div class="info">
				<h2><?php the_field('get_in_touch_channel_headline', 'options'); ?></h2>
				<?php the_field('get_in_touch_channel_deck', 'options'); ?>
			</div>
			
		</div>	
	</section>