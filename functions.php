<?php

// Theme Support
add_theme_support( 'post-thumbnails' );

// Filters
add_filter('the_content', 'filter_ptags_on_images');

function the_slug() {
	global $post;
	$post_data = get_post($post->ID, ARRAY_A);
	$slug = $post_data['post_name'];
	return $slug;
}

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

show_admin_bar(false);


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}


add_filter('upload_mimes', 'custom_upload_mimes');

function custom_upload_mimes ( $existing_mimes=array() ) {

	// add the file extension to the array

	$existing_mimes['svg'] = 'mime/type';

        // call the modified list of extensions

	return $existing_mimes;

}

function entry_tags() {
	
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug . ' '; 
	  }
	}

}