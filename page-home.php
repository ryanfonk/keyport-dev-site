<?php get_header(); ?>

	<section id="hero" class="backstretch" data-img-src="<?php $image = get_field('hero_image'); echo $image['url']; ?>">

		<div class="wrapper">

			<h1><?php the_field('hero_headline'); ?></h1>

		</div>

	</section>

	<section class="news">

		<div class="wrapper">

			<h2>News</h2>

			<ul class="bxslider">

				<?php $args = array('post_type' => 'post','post_status'=> 'publish','posts_per_page' => '9','order' => 'DSC'); ?>

				<?php $loop = new WP_Query($args); ?>

					<?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

						<li>

							<div class="slide-wrap">

								<p><?php echo get_the_date(); ?></p>

								<a href="<?php echo the_permalink(); ?>"><h6><?php the_title(); ?></h6></a>

								<div class="desc">

									<?php the_field('short_description'); ?>

									<a href="<?php echo the_permalink(); ?>">Read Article</a>

								</div>

							</div>

						</li>

				<?php endwhile; endif; ?>

			</ul>

		</div>

	</section>

	<section class="products">

		<div class="wrapper title">

			<h2>Our products</h2>

		</div>

		<?php include('inc/products.php'); ?>

	</section>

	<?php include('inc/get-in-touch.php'); ?>

<?php get_footer(); ?>
